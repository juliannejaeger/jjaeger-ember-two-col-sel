import { readOnly } from '@ember/object/computed';
import { A } from '@ember/array';
import FilterObject from '../utils/filter-object';

export default FilterObject.extend({
  init: function(){
    this._super();
    this.setProperties({
      type: "multi-select",
      label: "",
      name: "",
      selectedItems: A(),
      filterTargetDepth: 0,
      filterSelectContent: A(),
      defaultValue: null,
      optionValuePath: "content.name",
      optionLabelPath: "content.name"
    });
  },
  selectedItemsLength: readOnly('selectedItems.length', 'selectedItems', function(){
    return this.get('selectedItems.length');
  })
});
