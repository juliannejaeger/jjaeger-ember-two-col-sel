import Component from '@ember/component';
import $ from 'jquery';

export default Component.extend({

  tagName: '',
  iconList: null,
  iconClass: "",

  actions: {
    hidePopover(){
      $('#toggle-popover').click();
    },
    showModal(modalId){
      $('#'+modalId).modal('show');
    },
    closeModal(modalId){
      $('#'+modalId).modal('hide');
    }
  }
});
